#!/usr/bin/python

import bottle # http://bottlepy.org
import bottle_sqlite
import hashlib
import time
import yaml

with open('config.yaml') as f:
    config = yaml.safe_load(f)

# BACKEND

def get_fork(fork, db):
    """Retrieve a dict of data for a given fork, given its hash."""
    fields = ['id','hash','content','left_choice','left_fork',
              'right_choice','right_fork','created','creator']
    # Check to see if this fork exists
    fork_data = db.execute(
            "SELECT %s FROM forks WHERE hash = ?" % ",".join(fields),
            (fork,),
        ).fetchone()
    # Turn a successful result into a dict
    return dict(zip(fields, fork_data)) if fork_data else None

def put_fork(content, left_choice, right_choice, request, db):
    """Create a new fork with the specified values, and return the hash."""
    values = {
        'content': content,
        'left_choice': left_choice,
        'right_choice': right_choice,
        'created': int(time.time()),
        'creator': request.environ.get('REMOTE_ADDR'),
    }
    keys = values.keys() # Create this here to guarantee constant ordering

    result = db.execute(
            "INSERT INTO forks (%s) VALUES (%s)" % (",".join(keys), ",".join('?' for k in keys)),
            [values[k] for k in keys],
        )
    fork_id = result.lastrowid

    fork_hash = hashlib.md5('%s-%s' % (config['secret'], fork_id)).hexdigest()
    db.execute("UPDATE forks SET hash = ? WHERE id = ?", (fork_hash, fork_id))

    return fork_hash

def update_fork(fork_id, content, left_choice, right_choice, db):
    """Update an existing fork with new values."""
    db.execute(
        "UPDATE forks SET content=?, left_choice=?, right_choice=? WHERE id=?",
        (content, left_choice, right_choice, fork_id),
    )

def wordwrap(text, width):
    """
    A word-wrap function that preserves existing line breaks
    and most spaces in the text. Expects that existing line
    breaks are posix newlines (\n).

    Source: http://code.activestate.com/recipes/148061-one-liner-word-wrap-function/
    """
    return reduce(lambda line, word, width=width: '%s%s%s' %
                  (line,
                   ' \n'[(len(line)-line.rfind('\n')-1
                         + len(word.split('\n',1)[0]
                              ) >= width)],
                   word),
                  text.split(' ')
                 )

def validate_fork(content, left_choice, right_choice, honeypot):
    """Checks form fields for creating/editing a fork."""

    if honeypot:
        bottle.abort(403, "No spam, please.")

    if not (content and left_choice and right_choice):
        bottle.abort(403, "One or more required fields were missing. Please go back and try again.")

    if len(content) > 10000:
        bottle.abort(403, "The supplied description is too long. (10,000 characters max.)")

    if len(content) < 30:
        bottle.abort(403, "The supplied description is too short. (30 characters min.)")

    if len(left_choice) > 200 or len(right_choice) > 200:
        bottle.abort(403, "One or more of the choices are too long. (200 characters each max.)")

    for s in config['illegal_strings']:
        if s in content or s in left_choice or s in right_choice:
            bottle.abort(403, "The supplied values cannot contain '%s'." % s)

# WEBAPP

app = bottle.Bottle()
app.install(bottle_sqlite.Plugin(dbfile=config['db']))

@app.route('/')
def index(db):
    start_fork = db.execute("SELECT hash FROM forks WHERE id = 1").fetchone()
    if not start_fork:
        bottle.abort(500, "Couldn't find a starting fork!")

    bottle.redirect('/%s' % start_fork[0], 302)

@app.route('/about')
def about(db):
    count = db.execute("SELECT count(*) FROM forks").fetchone()[0]
    return bottle.template('about', fork_count=count)

@app.route('/go/:choice/:fork')
def go(choice, fork, db):
    from_fork = get_fork(fork, db)
    if not from_fork:
        bottle.abort(404, "The requested page was not found.")

    if choice == 'left':
        chosen_fork = from_fork['left_fork']
        chosen_text = from_fork['left_choice']
    elif choice == 'right':
        chosen_fork = from_fork['right_fork']
        chosen_text = from_fork['right_choice']
    else:
        bottle.abort(404, "The requested page was not found.")

    if not chosen_fork:
        return bottle.template('create', prev_fork=from_fork, choice=choice, chosen_text=chosen_text)
    
    bottle.redirect('/%s' % chosen_fork, 302)

@app.route('/create/:choice/:fork', method="POST")
def create(choice, fork, db):
    from_fork = get_fork(fork, db)
    if not from_fork or choice not in ('left','right'):
        bottle.abort(404, "The requested page was not found.")

    if choice == 'left' and from_fork['left_fork']:
        bottle.abort(403, "That fork already exists!")
    elif choice == 'right' and from_fork['right_fork']:
        bottle.abort(403, "That fork already exists!")

    honeypot = bottle.request.forms.get('pot')
    content = bottle.request.forms.get('content')
    left_choice = bottle.request.forms.get('left')
    right_choice = bottle.request.forms.get('right')

    validate_fork(content, left_choice, right_choice, honeypot)

    new_fork = put_fork(content, left_choice, right_choice, bottle.request, db)
    db.execute("UPDATE forks SET %s_fork = ? WHERE id = ?" % choice, (new_fork, from_fork['id']))
    db.commit()

    bottle.redirect('/%s' % new_fork, 302)

@app.route('/edit/:fork')
def edit(fork, db):
    existing_fork = get_fork(fork, db)
    if not existing_fork:
        bottle.abort(404, "The requested page was not found.")

    if existing_fork['creator'] != bottle.request.environ.get('REMOTE_ADDR'):
        bottle.abort(403, "You are not allowed to edit this plot fork (IP mismatch).")

    if existing_fork['left_fork'] or existing_fork['right_fork']:
        bottle.abort(403, "You may no longer edit this fork because it has one or more children.")

    return bottle.template('edit', fork=existing_fork)


@app.route('/modify/:fork', method="POST")
def modify(fork, db):
    existing_fork = get_fork(fork, db)
    if not existing_fork:
        bottle.abort(404, "The requested page was not found.")

    if existing_fork['creator'] != bottle.request.environ.get('REMOTE_ADDR'):
        bottle.abort(403, "You are not allowed to edit this plot fork (IP mismatch).")

    if existing_fork['left_fork'] or existing_fork['right_fork']:
        bottle.abort(403, "You may no longer edit this fork because it has one or more children.")

    honeypot = bottle.request.forms.get('pot')
    content = bottle.request.forms.get('content')
    left_choice = bottle.request.forms.get('left')
    right_choice = bottle.request.forms.get('right')

    validate_fork(content, left_choice, right_choice, honeypot)

    update_fork(existing_fork['id'], content, left_choice, right_choice, db)
    db.commit()

    bottle.redirect('/%s' % existing_fork['hash'], 302)

@app.route('/:fork')
def fork(fork, db):
    fork_dict = get_fork(fork, db)

    if not fork_dict:
        bottle.abort(404, "The requested page was not found.")

    can_edit = True
    if fork_dict['creator'] != bottle.request.environ.get('REMOTE_ADDR'):
        can_edit = False
    if fork_dict['left_fork'] or fork_dict['right_fork']:
        can_edit = False

    return bottle.template('fork', fork=fork_dict, can_edit=can_edit, wordwrap=wordwrap)

if __name__ == '__main__':
    bottle.run(app, host='0.0.0.0', port='8888')
