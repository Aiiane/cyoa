%if can_edit:
<div id="editlink"><a href="/edit/{{ fork['hash'] }}">edit?</a></div>
%end
<pre class="fork-content">
{{ wordwrap(fork['content'], 115) }}
</pre>
<ul class="fork-choices">
    <li><a href="/go/left/{{ fork['hash'] }}">{{ fork['left_choice'] }}</a></li>
    <li><a href="/go/right/{{ fork['hash'] }}">{{ fork['right_choice'] }}</a></li>
</ul>
%rebase base
