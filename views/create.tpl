<p>
    You've reached a plot fork that hasn't been visited yet. That means you get to create it!
</p>
<p>
    <em>Just remember: make sure to proofread your text before clicking 'Save'!</em>
</p>
<p>
    For reference, the choice that you selected (from <a href="/{{ prev_fork['hash'] }}">here</a>) was:
    <em>{{ chosen_text }}</em>
</p>
<form action="/create/{{ choice }}/{{ prev_fork['hash'] }}" method="POST">
<fieldset id="create-fork-form">
<label for="fork-content">Description (30 characters min., 10,000 characters max.)</label>
<textarea id="fork-content" name="content" maxlength="10000"></textarea>
<label for="fork-left-choice">Choice #1 (200 characters max.)</label>
<input id="fork-left-choice" name="left" maxlength="200" /><br/>
<label for="fork-right-choice">Choice #2 (200 characters max.)</label>
<input id="fork-right-choice" name="right" maxlength="200" />
<label id="fork-pot-label" for="fork-pot">Don't put anything in this field</label>
<input id="fork-pot" name="pot" />
<input type="submit" value="Save" />
</fieldset>
</form>
%rebase base
