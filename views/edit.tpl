<p>
    You are currently editing an existing plot fork (<a href="/{{ fork['hash'] }}">this one</a>).
</p>
<p>
    <em>Remember: make sure to proofread your text before clicking 'Save'!</em>
</p>
<form action="/modify/{{ fork['hash'] }}" method="POST">
<fieldset id="create-fork-form">
<label for="fork-content">Description (30 characters min., 10,000 characters max.)</label>
<textarea id="fork-content" name="content" maxlength="10000">{{ fork['content'] }}</textarea>
<label for="fork-left-choice">Choice #1 (200 characters max.)</label>
<input id="fork-left-choice" name="left" maxlength="200" value="{{ fork['left_choice'] }}" /><br/>
<label for="fork-right-choice">Choice #2 (200 characters max.)</label>
<input id="fork-right-choice" name="right" maxlength="200" value="{{ fork['right_choice'] }}" />
<label id="fork-pot-label" for="fork-pot">Don't put anything in this field</label>
<input id="fork-pot" name="pot" />
<input type="submit" value="Save" />
</fieldset>
</form>
%rebase base
