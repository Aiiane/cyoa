<p>The goal of this project is to create a completely user-generated adventure.
Work your way through the plot forks until you reach an unexplored frontier, at
which point you'll be asked to help build out from there.</p>
<p>There are currently {{ fork_count }} plot fork(s) in the database.</p>
<p>Note: contributors' IP addresses are recorded along with their contributions. (This record purely for help in combatting potential spammers and will not be disclosed to any outside party. IP addresses are not publicly displayed.)</p>
<p>This app was created by <a href="http://aiiane.com/+">Amber Yust</a>. You can find the source <a href="http://git.aiiane.com/cyoa">here</a>.</p>
%rebase base
