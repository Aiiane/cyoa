<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>CYOA</title>
	<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=iso-8859-1" />
	<link href="/static/css/base.css" rel="stylesheet" type="text/css" />
</head>

<body>
	<div id="container">
		<ul id="nav">
				<li><a href="/" title="Start from the beginning.">Start</a></li>
				<li><a href="/about" title="Learn more about this web app.">About</a></li>
		</ul>
		<div id="content">
            %include
		</div>	
		<div id="footer">
            <a rel="license" href="http://creativecommons.org/publicdomain/mark/1.0/">
                <img src="http://i.creativecommons.org/p/mark/1.0/80x15.png"
                style="border-style: none;" alt="Public Domain Mark" />
            </a>
		</div>
	</div>
</body>
</html>
